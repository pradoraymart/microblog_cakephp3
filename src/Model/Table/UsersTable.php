<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->setTable('users');
        $this->setPrimaryKey('id');
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Follows', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommentAuthors', [
            'foreignKey' => 'user_id',
            'conditions' => 'Comments.user_id'
        ]);
        $this->hasMany('Posts')
            ->setName('AuthorRetweets')
            ->setforeignKey('author_id')
            ->setDependent(true);
        $this->belongsTo('Comments')
            ->setforeignKey('Comments.user_id');
    }

    /**
     * Validation Default
     *
     * @param Validator $validator
     * @return $validator
     */
    public function validationRegister(Validator $validator)
    {
        $validator
            ->requirePresence('first_name', __('First name is required'))
            ->notEmpty('first_name', 'Please, enter your name !')
            ->add('first_name', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-Z\.\s]*$/i'],
                'message' => __('Please enter letter characters only')
            ])
            ->add('first_name', [
                'length' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'Names must not be more than 50 characters'
                ]
            ])
            ->allowEmptyString('last_name', 'First name is required')
            ->add('last_name', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-Z\.\s]*$/i'],
                'message' => 'Please enter letter characters only'
            ])
            ->add('last_name', 'length', [
                'rule' => ['maxLength', 50],
                'message' => 'Names must not be more than 50 characters'
            ])
            ->notEmpty('username', 'A username is required')
            ->add('username', 'length', [
                'rule' => ['lengthbetween', 5, 15],
                'message' => 'Usernames must be between 5 to 15 characters'
            ])
            ->add('username', 'validFormat', [
                'rule' => ['custom', '/^[a-z0-9]{3,}$/i'],
                'message' => 'Username can only be letters and numbers'
            ])
            ->add('username', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This username is already taken'
                    ]
            ])
            ->notEmpty('email', 'A email is required')
            ->requirePresence('email', 'create')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Invalid email format'
            ])
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This email is already used'
                    ]
            ])
            ->notEmpty('password', 'Password is required')
            ->add('password', 'validFormat', [
                'rule' => ['custom', '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/'],
                'message' => 'Password must contain 8 characters and have at least one letter and one number'
            ])
            ->requirePresence('confirm_password', 'create')
            ->notEmpty('confirm_password',  'Confirm Password is required')
            ->allowEmpty('confirm_password', 'update')
            ->add('confirm_password', 'compareWith', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords do not match.'
            ])
            ->notEmpty('gender', 'Please choose a gender')
            ->inList('gender', [0, 1], 'Please choose either Male or Female only')
            ->notEmpty('date_of_birth', 'Birth date is required')
            ->add('date_of_birth', [
                'custom' => [
                    'rule' => 'isOver14',
                    'provider' => 'table',
                    'message' => 'You must be 14 years old and above to register'
                ]
            ])
            ->allowEmpty('self_description')
            ->add('self_description', [
                'length' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'Bio must not be more than 50 characters'
                ]
            ])
            ->allowEmpty('image')
            ->add('image', [
                'validExtension' => [
                    'rule' =>
                        ['extension', [
                            'png', 'jpg', 'gif', 'jpeg'
                            ]
                        ],
                    'message' => 'Please only upload images (gif, png, jpg).'
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '5MB'],
                    'message' => 'Cover image must be less than 5MB.',
                    'allowEmpty' => true,
                ],
            ]);

        return $validator;
    }

    /**
     * User Login Validation
     *
     * @return validator $validator
     */
    public function validationLogin(Validator $validator)
    {
        $validator
            // User Email Validation
            ->notEmpty('username', 'A username is required')
            // User Password Validation
            ->notEmpty('password', 'Password is required');

        return $validator;
    }

    /**
     * User Edit Profile Validation
     *
     * @return validator $validator
     */
    public function validationUpdate(Validator $validator)
    {
        $validator
            ->requirePresence('first_name', __('First name is required'))
            ->notEmpty('first_name', 'Please, enter your name !')
            ->add('first_name', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-Z\.\s]*$/i'],
                'message' => __('Please enter letter characters only')
            ])
            ->add('first_name', [
                'length' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'Names must not be more than 50 characters'
                ]
            ])
            ->requirePresence('last_name', __('Last name is required'))
            ->add('last_name', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-Z\.\s]*$/i'],
                'message' => 'Please enter letter characters only'
            ])
            ->add('last_name', 'length', [
                'rule' => ['maxLength', 50],
                'message' => 'Names must not be more than 50 characters'
            ])
            ->notEmpty('username', 'A username is required')
            ->add('username', 'length', [
                'rule' => ['lengthbetween', 5, 15],
                'message' => 'Usernames must be between 5 to 15 characters'
            ])
            ->add('username', 'validFormat', [
                'rule' => ['custom', '/^[a-z0-9]{3,}$/i'],
                'message' => 'Username can only be letters and numbers'
            ])
            ->add('username', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This username is already taken'
                    ]
            ])
            ->allowEmpty('new_password')
            ->add('new_password', 'length', [
                'rule' => ['minLength', '8'],
                'message' => 'Password must have a mimimum of 8 characters'
            ])
            ->allowEmpty('confirm_password')
            ->add('confirm_password', 'compareWith', [
                'rule' => ['compareWith', 'new_password'],
                'message' => 'Passwords do not match.'
            ])
            ->notEmpty('gender', 'Please choose a gender')
            ->inList('gender', [0, 1], 'Please choose either Male or Female only')
            ->notEmpty('date_of_birth', 'Birth date is required')
            ->allowEmpty('self_description')
            ->add('self_description', [
                'length' => [
                    'rule' => ['maxLength', '140'],
                    'message' => 'Bio must not be more than 140 characters'
                ]
            ])
            ->allowEmpty('image')
            ->add('image', [
                'validExtension' => [
                    'rule' => ['extension', ['png', 'jpg', 'gif', 'jpeg']],
                    'message' => 'Please only upload images (gif, png, jpg).'
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '5MB'],
                    'message' => 'Cover image must be less than 5MB.',
                    'allowEmpty' => true,
                ],
            ]);

        return $validator;
    }
    /**
     * Check user age if above 18 years old
     *
     * @param bool $check
     * @return bool true to show validation error if user is not above 18 years old
     */
    public function isOver14($check = [])
    {
        $bday = strtotime(implode($check));
        // check age
        if (time() < strtotime('+14 years', $bday)) {
            return false;
        }

        return true;
    }
}
