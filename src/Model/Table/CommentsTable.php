<?php
// src/Model/Table/CommentsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommentsTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Posts', [
            'className' => 'Posts',
            'foreignKey' => 'post_id'
        ]);
        $this->belongsTo('AuthorComments', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('content', 'Please insert a comment first')
            ->add('content', 'length', [
                'rule' => ['maxLength', '140'],
                'message' => 'Comment must not exceed to 140 characters'
            ]);

        return $validator;
    }
}
