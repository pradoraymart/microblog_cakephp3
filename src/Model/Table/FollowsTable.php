<?php
// src/Model/Table/FollowsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class FollowsTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Following', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
    }
}
