<?php
// src/Model/Table/PostsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Retweets', [
            'className' => 'Posts',
            'foreignKey' => 'post_id'
        ]);
        $this->belongsTo('AuthorRetweets', [
            'className' => 'Users',
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('CountRetweets', [
            'className' => 'Posts',
            'foreignKey' => 'post_id',
            'conditions' =>
                ['CountRetweets.is_deleted' => '0']
        ]);
        $this->hasMany('Comments')
            ->setForeignKey('post_id')
            ->setDependent(true)
            ->setConditions(['Comments.is_deleted' => 0]);
        $this->hasMany('Likes')
            ->setForeignKey('post_id')
            ->setConditions(['Likes.status' => 1]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('content')
            ->add('content', 'length', [
                'rule' => ['maxLength', '140'],
                'message' => 'Tweet must not be more than 140 characters'
            ])
            ->allowEmpty('image')
            ->add('image', [
                'validExtension' => [
                    'rule' => ['extension', ['png', 'jpg', 'gif', 'jpeg']],
                    'message' => 'Please only upload images (gif, png, jpg).'
                ],
                'fileSize' => [
                    'rule' => array('fileSize', '<=', '5MB'),
                    'message' => 'Cover image must be less than 5MB.',
                    'allowEmpty' => TRUE,
                ],
            ]);

        return $validator;
    }
}
