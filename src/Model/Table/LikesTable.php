<?php
// src/Model/Table/LikesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class LikesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Posts', [
            'className' => 'Posts',
            'foreignKey' => 'post_id'
        ]);
    }
}
