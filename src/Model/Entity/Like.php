<?php
// src/Model/Entity/Like.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Like extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'slug' => false,
    ];
}
