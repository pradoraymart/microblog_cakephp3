<?php
// src/Model/Entity/Follow.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Follow extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'slug' => false,
    ];
}
