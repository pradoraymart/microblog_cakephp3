<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

header('content-type: application/json');
header('Access-Control-Allow-Origin: *');
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("X-XSS-Protection: 1; mode=block");
header("Expires: 0");

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
<?php echo $this->Html->charset(); ?>
    <title>
    <?php echo 'Microblog' ?>:
    <?php echo $this->fetch('title'); ?>
    </title>
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css(['style', 'bootstrap.min', 'font-awesome.min']);

        echo $this->fetch('meta');
        echo $this->fetch('css');

        echo $this->Html->script(['jquery', 'jquery-3.4.1.min', 'bootstrap.min']);
        echo $this->fetch('script');
    ?>
</head>
<body>
    <?= $this->element('navbar');?>

    <div id="container">
        <div id="header">
        </div>
        <div id="content">
            <?php echo $this->Flash->render(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <div id="footer">
            <!-- Footer -->
            <footer class="page-footer font-small blue pt-4">
            <!-- Copyright -->
                <div class="footer-copyright text-center py-3">© 2019 Copyright:
                <a href="http://dev1.ynsdev.pw">Microblog</a>
                </div>
            <!-- Copyright -->
            </footer>
            <!-- Footer -->
        </div>
        <!-- /Footer -->
    </div>
<script>

$(function() {
    //call function that counts the character tweets
    retweetCounter();

    // Show and hide comments
    $(document).on("click", ".commentShow", function(e) {
        let id = $(this).attr("id");
        e.preventDefault();
        let target = jQuery(".comment-sec" + id);

        if (target.is(':visible')) {
            $('.comment-sec' + id).css('cssText', 'display: none !important');
        } else {
            $('.comment-sec' + id).css('cssText', 'display: block !important');
        }
    });

    // delete comments
    $(document).on('click','.delButton', function(e) {
        //prevent page from loading
        e.preventDefault();
        let element = $(this);
        let del_id = element.attr("id");
        let post_id = element.attr("data-id");
        let count = parseInt(element.attr("count")) + 1;

        if (confirm("Are you sure you want to delete this Comment?")) {
            $.ajax({
                type: 'POST',
                url: "../../comments/delete/" + del_id,
                headers: {'X-CSRF-Token': '<?= h($this->request->getParam('_csrfToken')); ?>'},
                complete: function() {
                    let totalComments = parseInt($('.countComments' + post_id).text());
                    let subComment = totalComments - 1;
                    if (subComment == 3) {
                        $("#viewAll" + post_id).remove();
                        $("#comment" + del_id).remove();
                        $('.countComments' + post_id).text(subComment);
                        $('.delCount' + post_id).attr("count", count);
                        window.location.reload();
                    } else if (count == 3) {
                        window.location.reload();
                    } else {
                        $("#comment" + del_id).remove();
                        $('.countComments' + post_id).text(subComment);
                        $('.delCount' + post_id).attr("count", count);
                    }
                }
            });
        }

        return false;
    });

    //Edit Comments
    $("#editComment").submit(function(e) {
        //prevent page from loading
        e.preventDefault();
        //disable send button after submit
        let formData = $('form.editComment').serialize();
        let commentPostId = $('#commentId').val();
        let commentContent = $("#commentContent").val();

        $("#submitEditedComment").attr("disabled", true);

        if (commentContent == '') {
            $('textarea[name=content').prop('required', true);
            $("#submitEditedComment").attr("disabled", false);
        } else {
            $.ajax({
                type: 'POST',
                url: "../../comments/edit/" + commentPostId,
                data: formData,
                headers: {'X-CSRF-Token': '<?= h($this->request->getParam('_csrfToken')); ?>'},
                complete: function(data, textStatus, xhr) {
                    $('#editComments').modal('hide');
                    $('textarea[name=content').prop('required', true);
                    $('.editButton#' + commentPostId).attr("data-id", commentContent);
                    $("#submitEditedComment").attr("disabled", false);
                    $('#contentId' + commentPostId).text(commentContent);
                }
            });
        }
        return false;
    });

    $(document).on('click','.editButton',function(e) {
        //prevent page from loading
        e.preventDefault();
        let element = $(this);
        let commentId = element.attr("id");
        let commentContent = element.attr("data-id");

        $("#commentId").val(commentId);
        $("#commentContent").val(commentContent);
        $('#editComments').modal('show');
    });
});

function likePost(id) {
    let totalLikes = parseInt($('.countLikes' + id).text());
    let addLike = totalLikes + 1;

    $.ajax({
        type: 'GET',
        url: "../../posts/like/" + id,
        success: function(data) {
            $('.commentLabel' + id).css('cssText', 'color: #e44d3a !important');
            $('.commentLabel' + id).attr("onclick", "unlikePost("+ id +")");
            $('.countLikes' + id).text(addLike);
        },
        error: function(data) {
        }
    });
}

function unlikePost(id) {
    let totalLikes = parseInt($('.countLikes' + id).text());
    let subLike = totalLikes - 1;

    $.ajax({
        type: 'GET',
        url: "../../posts/unlike/" + id,
        success: function(data) {
            $('.commentLabel' + id).css('cssText', 'color: #b2b2b2 !important');
            $('.commentLabel' + id).attr("onclick", "likePost("+ id +")");
            $('.countLikes' + id).text(subLike);
        },
        error: function(data){
        }
    });
}

function retweetCounter() {
    let maxLength = 140;
    $('#PostContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
    $('#PostRetweetContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
    $('#commentContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#commentRemainingCharacter').text(textlen);
    });
    $('#UserSelfDescription').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
}

</script>
    </body>
</html>
