<div class="main-section-data">
    <div class="row">
        <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
            <div id="posts-list" class="main-ws-sec">
                <div class="posts-section">
                <h4> People </h4>
                <?php if(sizeOf($user) > 0) { ?>
                    <?php foreach ($user as $users): ?>
                        <div class="post-bar">
                            <div class="post_topbar">
                                <div class="usy-dt">
                                    <?= $this->Html->image("profile/" . $users->image);?>
                                    <div class="usy-name">
                                        <h3>
                                            <?php
                                                $id = $users->id;
                                                echo h($users->first_name). " " . h($users->last_name);?>
                                        </h3>
                                        <small class="text-muted"><?= h($users->username);?></small>
                                    </div>
                                    <!--/ usy-name -->
                                </div>
                                <!--/ usy-dt -->
                                <div class="ed-user-list">
                                    <?= $this->Html->link('View Profile', [
                                            'controller' => 'users',
                                            'action' => 'view',
                                            $id
                                        ],
                                        [
                                            'escape' => false,
                                            'class' => 'btn btn-primary'
                                        ]
                                    ); ?>
                                </div>
                                <!--/ ed-opts -->
                            </div>
                            <!--/ post_topbar -->
                        </div>
                        <!-- / post-bar -->
                    <?php endforeach; ?>
                    <?php } else { ?>
                        <div class="row justify-content-center align-items-center">
                            No Users Found
                        </div>
                    <?php } ?>
                </div>

                <h4> Posts </h4>
                <?php if (sizeOf($posts) > 0) { ?>
                <?php foreach ($posts as $post): ?>
                <div class="post-bar">
                    <div class="post_topbar">
                        <div class="usy-dt">
                            <?= $this->Html->image("profile/" . h($post->user->image));?>
                            <div class="usy-name">
                                <h3>
                                    <?php
                                        $userId = $post->user->id;
                                        echo $this->Html->link(h($post->user->first_name). " " . h($post->user->last_name), [
                                            'controller' => 'users',
                                            'action' => 'view',
                                            $userId
                                        ]);
                                    ?>
                                </h3>
                                <span>
                                    <i class="fa fa-clock-o"></i>
                                    <?= $this->Time->format(h($post->created));?>
                                </span>
                            </div>
                            <!-- / usy-name -->
                        </div>
                        <!-- / usy-dt -->
                        <div class="ed-opts">
                            <?php
                            $postId = $post->id;
                            if ($authUser['id'] == $post->user_id) {?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', [
                                        'controller' => 'posts',
                                        'action' => 'edit',
                                        $postId], [
                                        'escape' => false,
                                        'title' => __('Edit')
                                    ]);
                                ?>
                                <?= $this->Form->postLink('<i class="fa fa-trash"></i>', [
                                        'controller' => 'posts',
                                        'action' => 'delete',
                                        $postId], [
                                        'escape' => false,
                                        'confirm' => 'Are you sure you want to delete this post?'
                                    ])
                                ?>
                            <?php } ?>
                        </div>
                        <!-- / ed-opts -->
                    </div>
                    <!-- / post_topbar -->
                    <?php if(isset($post->post_id)) { ?>
                        <div class="post_content" onClick="document.location.href='../posts/view/<?=$postId?>'">
                            <p> <?= h($post->content)?> </p>
                            <div class="retweet-bar col-md-11 offset-md-1">
                                <div class="retweet_bar">
                                    <div class="post_topbar">
                                        <div class="usy-dt">
                                            <?= $this->Html->image("profile/" . h($post->author_retweet->image));?>
                                            <div class="usy-name">
                                                <h6>
                                                    <?php
                                                        $authorId = $post->author_retweet->id;
                                                        echo $this->Html->link(h($post->author_retweet->first_name). " " . h($post->author_retweet->last_name), [
                                                                'controller' => 'users',
                                                                'action' => 'view',
                                                                $authorId
                                                            ]);
                                                    ?>
                                                </h6>
                                                <span>
                                                    <i class="fa fa-clock-o"></i>
                                                    <?= $this->Time->format(h($post->retweet->created)) ?>
                                                </span>
                                            </div>
                                            <!-- / usy-name -->
                                        </div>
                                        <!-- / usy-dt -->
                                    </div>
                                    <!-- / post_topbar -->
                                    <div class="post_content" onClick="document.location.href='../posts/view/<?=$post->post_id?>'">
                                    <?php
                                        // check post text and image presence
                                        if (empty($post->image)) {
                                            // check the post origin if has been deleted
                                            if ($post->retweet->is_deleted == 1) {
                                                echo '<p style="color:#e44d3a;"> Content is not available</p>';
                                            } else {
                                                echo '<p>';
                                                    echo h($post->retweet_content);
                                                echo '</p>';
                                            }
                                        } else {
                                            if ($post->retweet->is_deleted == 1) {
                                                echo '<p style="color:#e44d3a;"> Content Not Available</p>';
                                            } else {
                                                echo '<p>';
                                                    echo h($post->retweet_content);
                                                echo '</p>';
                                                echo $this->Html->image("posts/" . h($post->image));
                                            }
                                        }?>
                                    </div>
                                    <!-- / post-content -->
                                </div>
                                <!-- / retweet-bar -->
                            </div>
                            <!--/ retweet-bar col-md-11 -->
                        </div>
                        <!--/ post content -->
                    <?php } else { ?>
                        <div class="post_content" onClick="document.location.href='../posts/view/<?=$post->id?>'">
                            <?php
                                if (empty($post->image)) {
                                    echo '<p>';
                                        echo h($post->content);
                                    echo '</p>';
                                } else {
                                    echo '<p>';
                                        echo h($post->content);
                                    echo '</p>';
                                    echo $this->Html->image("posts/" . h($post->image));
                                }?>
                        </div>
                        <!--/ post content -->
                        <?php } ?>
                        <div id="statusSection" class="job-status-bar">
                            <ul class="like-com">
                                <?php $likeId = $postId;?>
                                <li style="margin-right: 0px !important;">
                                    <p class="countLikes<?=$likeId?>"><?= sizeof($post->likes);?></p>
                                </li>
                                <?php
                                    $isLiked = false;
                                    for ($i = 0; $i < sizeof($post->likes); $i++) {
                                        $isLiked = ($post['likes'][$i]['user_id'] == $authUser['id'] ? true : false);
                                    }
                                    if ($isLiked) { ?>
                                        <li class="btnLike">
                                            <p style="color:#e44d3a !important" class="com comLabel commentLabel<?=$likeId?>" onclick="unlikePost(<?=$likeId?>);">
                                                <i class="fa fa-thumbs-up"></i>
                                                Like
                                            </p>
                                        </li>
                                    <?php } else { ?>
                                    <li class="btnLike">
                                        <p class="com comLabel commentLabel<?=$likeId?>" onclick="likePost(<?=$likeId?>);">
                                            <i class="fa fa-thumbs-up"></i>
                                            Like
                                        </p>
                                    </li>
                                <?php } ?>
                                <li style="margin-right: 0px !important;">
                                    <p class="countComments<?=$postId?>">
                                        <?= sizeof($post->comments)?>
                                    </p>
                                </li>
                                <li>
                                    <a id="<?=$postId?>" class="com commentShow" >
                                        <i class="fa fa-comment"></i>
                                            Comment
                                    </a>
                                </li>
                                <li>
                                    <?= $this->Html->link('<i class="fa fa-retweet"></i> ' .sizeof($post->count_retweets). ' Retweet', [
                                            'controller' => 'posts',
                                            'action' => 'retweet',
                                            $postId], [
                                            'escape' => false,
                                            'class' => 'com'
                                        ]);
                                    ?>
                                </li>
                            </ul>
                        </div>
                        <!--/ #statusSection -->
                    </div>
                    <!--/ post-bar -->
                    <div class="comment-section" style="padding-left:15px !important;">
                        <div id="updateComment<?=$postId?>">
                        <?php
                            $totalComments = sizeof($post->comments);
                            if ($totalComments >= 3) {
                                $limitcomments = $totalComments - 3;
                                if ($totalComments > 3): ?>
                                <div class="plus-ic" id="viewAll<?=$postId?>">
                                    <?= $this->Html->Link('View All Comments', [
                                        'controller'=> 'posts',
                                        'action' => 'view',
                                        $postId], [
                                        'title' => 'View all comments',
                                        'escape' => false
                                        ]);
                                    ?>
                                </div>
                            <?php
                            endif;
                            for ($i = $limitcomments; $i < $totalComments; $i++) {?>
                                <div class="comment-sec<?=$postId ?>" id="comment<?=$post['comments'][$i]['id']?>">
                                    <ul>
                                        <li>
                                            <div id="commentSection" class="comment-list">
                                                <div class="bg-img">
                                                    <?= $this->Html->image("profile/" . h($post['comments'][$i]['author_comment']['image']));?>
                                                </div>
                                                <div class="comment">
                                                    <h3>
                                                        <?php
                                                            $commentUserId = $post['comments'][$i]['author_comment']['id'];
                                                            echo $this->Html->link(h($post['comments'][$i]['author_comment']['first_name']) . " " . h($post['comments'][$i]['author_comment']['last_name']), [
                                                                    'controller' => 'users',
                                                                    'action' => 'view',
                                                                    $commentUserId
                                                                ]);
                                                            if ($authUser['id'] == $post['comments'][$i]['user_id']) {
                                                                $commentId = $post['comments'][$i]['id'];?>
                                                                <i id="<?= $commentId ?>" data-id="<?= h($post['comments'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                                                <?= $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                                                        'escape' => false,
                                                                        'id' => $commentId,
                                                                        'data-id' => $post['id'],
                                                                        'count' => 0,
                                                                        'class' => 'delButton delBtn delCount'.$postId
                                                                    ]);
                                                                ?>
                                                            <?php } ?>
                                                    </h3>
                                                    <span>
                                                        <i class="fa fa-clock-o"></i>
                                                        <?= $this->Time->format(h($post['comments'][$i]['created']))?>
                                                    </span>
                                                    <div class="comment-list-content">
                                                        <p id="contentId<?=$post['comments'][$i]['id']?>">
                                                           <?= h($post['comments'][$i]['content'])?>
                                                        </p>
                                                    <div>
                                                </div>
                                            </div><!--comment-list end-->
                                        </li>
                                    </ul>
                                </div><!--comment-sec end-->
                            <?php } ?>
                        <?php } ?>
                        <?php
                         if ($totalComments < 3) {
                                for ($i = 0; $i < $totalComments; $i++) {
                                ?>
                                <div class="comment-sec<?=$postId ?>" id="comment<?=$post['comments'][$i]['id']?>">
                                    <ul>
                                        <li>
                                            <div id="commentSection" class="comment-list">
                                                <div class="bg-img">
                                                    <?= $this->Html->image("profile/" . h($post['comments'][$i]['author_comment']['image']));?>
                                                </div>
                                                <div class="comment">
                                                    <h3>
                                                    <?php
                                                        $commentUserId = $post['comments'][$i]['users']['id'];
                                                        echo $this->Html->link(h($post['comments'][$i]['author_comment']['first_name']) . " " . h($post['comments'][$i]['author_comment']['last_name']), [
                                                                'controller' => 'users',
                                                                'action' => 'view',
                                                                $commentUserId
                                                            ]);

                                                        if ($authUser['id'] ==$post['comments'][$i]['user_id']) {
                                                            $commentId = $post['comments'][$i]['id'];?>
                                                            <i id="<?= $commentId ?>" data-id="<?= h($post['comments'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                                            <?php
                                                                echo $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                                                        'escape' => false,
                                                                        'id' => $commentId,
                                                                        'data-id' => $post['id'],
                                                                        'class' => 'delButton delBtn'
                                                                    ]);
                                                            ?>
                                                        <?php } ?>
                                                    </h3>
                                                    <span> <i class="fa fa-clock-o"></i> <?= $this->Time->format(h($post['comments'][$i]['created']))?> </span>
                                                    <div class="comment-list-content">
                                                        <p id="contentId<?=$post['comments'][$i]['id']?>">
                                                           <?= h($post['comments'][$i]['content'])?>
                                                        </p>
                                                    <div>
                                                </div>
                                            </div><!--comment-list end-->
                                        </li>
                                    </ul>
                                </div><!--comment-sec end-->
                            <?php } ?>
                        <?php } ?>
                        </div><!--/ updateComment -->
                        <div class="row post-comment" style="padding-bottom:30px;">
                            <div class="bg-img">
                                <?= $this->Html->image("profile/". $authUser['image']);?>
                            </div>
                            <div class="comment_box">
                                <?= $this->Form->create('Comment', [
                                        'type' => 'get',
                                        'id' => 'saveComment',
                                        'class' => 'saveComment'
                                    ])
                                ?>
                                <?= $this->Form->control('postid', [
                                        'type' => 'hidden',
                                        'value' => $post->id,
                                        'label' => false
                                    ])
                                ?>
                                <?= $this->Form->control('userid', [
                                        'type' => 'hidden',
                                        'value' => $authUser['id'],
                                        'label' => false
                                    ])
                                ?>
                                <?php
                                    $lastComment = end($comment); // get the last comment
                                    $comId = $lastComment['id'];
                                ?>
                                <?= $this->Form->control('commentid', [
                                    'type' => 'hidden',
                                    'value' => $comId,
                                    'count' => 1,
                                    'label' => false,
                                    'class' => 'countComment'.$post->id
                                    ])
                                ?>
                                <?= $this->Form->control('content', [
                                        'placeholder' => 'Post a comment',
                                        'maxlength' => 140,
                                        'label' => false
                                    ])
                                ?>
                            </div>
                            <div>
                                <?= $this->Form->control('Send', [
                                        'type' => 'submit',
                                        'label' => false,
                                        'id' => 'submitComment'.$post->id,
                                        'class'=>'comment_box2'
                                    ])
                                ?>
                                <?= $this->Form->end()?>
                            </div>
                        </div>
                        <!--/ post-comment end-->
                    </div>
                    <!--/ comment-section-->
                <?php endforeach; ?>
                <?php } else { ?>
                    <div class="row justify-content-center align-items-center">
                        No Posts Found
                    </div>
                <?php } ?>
            </div>
            <!--/ main-ws-sec -->

            <?php
                $paginator = $this->Paginator->setTemplates([
                        'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
                        'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
                        'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
                        'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
                        'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
                        'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>',
                ]);
            ?>
            <ul class="pagination">
                <?php
                    if (sizeof($posts) > 0) {
                        echo $paginator->first();
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev();
                        }
                        echo $paginator->numbers();
                        if ($paginator->hasNext()) {
                            echo $paginator->next();
                        }
                        echo $paginator->last();
                    } else {
                        echo 'No posts yet.';
                    }
                ?>
            </ul>
            <!--/ paging -->
        </div>
        <!--/ col-lg-6 col-md-8 no-pd -->
    </div>
    <!--/ row -->
</div>
<!--/ main-section -->

<!-- Edit Comment Modal -->
<!-- Modal -->
<div class="modal fade" id="editComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Edit Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </div>
            <!--/ modal-header -->
            <div class="modal-body">
                <?= $this->Form->create('Comment', [
                        'type' => 'get',
                        'id' => 'editComment',
                        'class' => 'editComment'
                    ]);
                ?>
                <?= $this->Form->input('postid', [
                        'id' => 'commentId',
                        'type' => 'hidden',
                        'label' => false
                    ]);
                ?>
                <div class="form-group">
                    <?= $this->Form->input('content', [
                            'type' => 'textarea',
                            'rows' => '3',
                            'cols' => '60',
                            'id' => 'commentContent',
                            'class' => 'form-control',
                            'maxlength' => 140,
                            'label' => false
                        ]);
                    ?>
                    <small class="form-text text-muted">
                        <span style="float:left;" id="commentRemainingCharacter">140</span>
                    </small>
                </div>
                <div class="form-group">
                    <?= $this->Form->input('Save Changes', [
                            'type' => 'submit',
                            'label' => false,
                            'id' => 'submitEditedComment',
                            'class'=>'comment_box2'
                        ]);
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <!--/ modal-body -->
        </div>
        <!--/ modal-content -->
    </div>
    <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<script type="text/javascript">

$(document).ready(function () {
    $(".saveComment").submit(function(e){

        //prevent page from loading
        e.preventDefault();
        let formData = $(this).serializeArray();
        let values = {};

        // get the values from serialize array
        $(formData).each(function(i, field){
            values[field.name] = field.value;
        });
        // Value Retrieval Function
        let getValue = function (valueName) {
            return values[valueName];
        };

        let id = getValue("postid");
        let userid = getValue("userid");
        let idcomment = getValue("commentid");
        let content = getValue("content");

        let countOrig = parseInt($('input[name=commentid').attr('count'));
        let commentId = parseInt(idcomment) + countOrig;
        let totalComments = parseInt($('.countComments' + id).text());
        let subComment = totalComments + 1;
        let count = parseInt($('input[name=commentid').attr('count')) + 1;

        // disable send button after submit
        $("#submitComment" + id).attr("disabled", true);

        // set field to required
        if(content == '') {
            $('input[name=content').prop('required', true);
            $("#submitComment" + id).attr("disabled", false);
        } else {
            $.ajax({
                type: 'POST',
                url: "../comments/add/",
                data: {postid: id, userid: userid, content: content},
                headers: {'X-CSRF-Token': '<?= h($this->request->getParam('_csrfToken')); ?>'},
                complete: function(data,textStatus,xhr) {
                    // increment count on #commentid input for the id of the next comment
                    $('.countComment' + id).attr("count", count);

                    // enable send button after submit
                    $("#submitComment" + id).attr("disabled", false);

                    // increment number of comment
                    $('.countComments' + id).text(subComment);

                    //reset input field
                    $('input[name=content').val('');
                    $('input[name=content').prop('required', true);

                    $('#updateComment' + id).append(`
                    <div class="comment-sec`+id+`" id="comment`+commentId+`">
                        <ul>
                            <li>
                                <div id="commentSection" class="comment-list">
                                    <div class="bg-img">
                                        <?= $this->Html->image("profile/". $authUser['image']);?>
                                    </div>
                                    <div class="comment">
                                        <h3>
                                        <?php
                                        echo $this->Html->link(h($authUser['first_name']) . " " . h($authUser['last_name']), [
                                                'controller' => 'users',
                                                'action' => 'view',
                                                $authUser['id']
                                        ]);?>
                                            <i id="`+commentId+`" data-id="` + content + `" class="editButton editBtn fa fa-edit"></i>
                                            <?= $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                                    'escape' => false,
                                                    'id' => "`+commentId+`",
                                                    'data-id' => "`+id+`",
                                                    'class' => 'delButton delBtn'
                                                ]);
                                            ?>
                                        </h3>
                                        <span>
                                            <i class="fa fa-clock-o"></i>
                                            <?= date("d.m.Y, H:i"); ?>
                                        </span>
                                        <div class="comment-list-content">
                                            <p id="contentId`+commentId+`">
                                            </p>
                                        <div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    `);
                    $('#contentId' + commentId).text(content);
                }
            });
        }

        return false;
    });
});
</script>
