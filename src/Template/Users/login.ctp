<br>
<?= $this->Flash->render(); ?>
<br>
<div class="row justify-content-center">
    <div class="col-md-4">
    <h3 style="text-align:center;color:#e44d3a;">MICROBLOG</h3><br>
        <div class="card">
            <header class="card-header">
                <h4 class="card-title mt-2">Log in</h4>
            </header>
            <div class="card-body">
                <?php $this->Form->setTemplates([
                        'inputContainer' => '<div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div>',
                        'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
                        'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
                        'error' => '<div class="text-danger">{{content}}</div>'
                    ]);
                ?>
                <?= $this->Form->create($login, ['url' => ['action' => 'login']]);?>
                <?= $this->Form->control('username'); ?>
                <?= $this->Form->control('password'); ?>
                <?= $this->Form->button('Login', [
                        'action' => 'login',
                        'label' => false,
                        'class' => 'btn btn-primary btn-block'
                    ]);
                ?>
                <?= $this->Form->end(); ?>
            </div> <!--/ card-body end -->
            <div class="border-top card-body text-center">Don't have account?
                <?= $this->Html->link('Register', ['action' => 'register']); ?>
            </div>
        </div> <!--/ card -->
    </div> <!--/ col -->
</div> <!--/ row -->
