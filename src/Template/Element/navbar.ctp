<?php if ($authUser): ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e44d3a !important;">
    <a class="navbar-brand" href="#"> Microblog </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <?php
                $controller = $this->request->getParam('controller');
                $action = $this->request->getParam('action');
            ?>
            <li class="nav-item <?php echo (($controller == 'posts') && ($action == 'index')) ? 'active' :'inactive' ?>">
                <?php
                    $userId = $authUser['id'];
                    echo $this->Html->link('<i class="fa fa-home"></i> Home <span class="sr-only">(current)</span>', [
                        'controller' => 'posts',
                        'action' => 'index'], [
                        'escape' => false,
                        'title' => __('home'),
                        'class' => 'nav-link'
                        ]);
                ?>
            </li>
            <!-- / navbar-item -->
            <li class="nav-item <?php echo (($controller == 'users') && ($action == 'view')) ? 'active' :'inactive' ?>">
                <?= $this->Html->link('<i class="fa fa-user"></i> Profile', [
                    'action' => 'view',
                    'controller' => 'users',
                    $userId ],[
                    'escape' => false,
                    'title' => __('Profile'),
                    'class' => 'nav-link'
                    ])
                ?>
            </li>
            <!-- / navbar-item -->
        </ul>
        <!-- / navbar-nav -->
    </div>
    <!-- / #navbarNavDropdown -->
    <div class="search-form">
        <?= $this->Form->create('User', [
                'type' => 'get',
                'url' => ['controller' => 'users', 'action' => 'search'],
                'class' => 'form-inline',
                'autocomplete' => 'off'
            ]);
        ?>
        <?= $this->Form->control('search', [
                'class' => 'form-control mr-sm-2',
                'placeholder' => 'Search',
                'label' => false
            ]);
        ?>
        <?= $this->Form->button('Search', [
                'action' => 'search',
                'controller' => 'users',
                'class' => 'btn btn-outline-success my-2 my-sm-0'
            ]);
        ?>
        <?= $this->Form->end(); ?>
    </div>
    <!--/ search-form  -->
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo h($authUser['first_name']). " " . h($authUser['last_name'])?>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <?php
                echo $this->Html->link('Edit Profile', [
                        'controller' => 'users',
                        'action' => 'edit'], [
                        'class' => 'dropdown-item'
                    ]);
                ?>
                <?= $this->Html->link('Logout', [
                    'controller' => 'users',
                    'action' => 'logout'], [
                    'class' => 'dropdown-item'
                    ]);
                ?>
            </div>
            <!--/ dropdown-menu-->
        </li>
        <!--/ nav-item dropdown -->
    </ul>
    <!--/ navbar-nav -->
    </nav>
    <!--/ navbar navbar-expand-lg navbar-light bg-light-->
    <?php endif; ?>
