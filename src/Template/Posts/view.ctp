<div class="main-section-data">
    <div class="row">
        <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
            <div id="posts-list" class="main-ws-sec">
                <?php foreach ($posts as $post): ?>
                <div class="posts-section">
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="usy-dt">
                                <?= $this->Html->image("profile/" . h($post->user->image)); ?>
                                <div class="usy-name">
                                    <h3>
                                        <?php
                                            $userId = h($post->user_id);
                                            echo $this->Html->link(h($post->user->first_name). " " . h($post->user->last_name), [
                                                    'controller' => 'users',
                                                    'action' => 'view',
                                                    $post->id
                                                ]);
                                        ?>
                                    </h3>
                                    <span>
                                        <i class="fa fa-clock-o"></i>
                                        <?= $this->Time->format(h($post->created))?>
                                    </span>
                                </div>
                                <!--/ usy-name -->
                            </div>
                            <!--/ usy-dt -->
                            <div class="ed-opts">
                                <?php
                                    $id = $post->id;
                                    if ($authUser['id'] == $userId) {
                                        echo $this->Html->link('<i class="fa fa-edit"></i>', [
                                                'action' => 'edit',
                                                $id],[
                                                'escape' => false,
                                                'title' => __('Edit')
                                            ]);
                                        echo $this->Form->postLink('<i class="fa fa-trash"></i>', [
                                                'controller' => 'posts',
                                                'action' => 'delete',
                                                $id], [
                                                'escape' => false,
                                                'confirm' => 'Are you sure you want to delete this post?'
                                            ]);
                                    }
                                ?>
                            </div>
                            <!--/ ed-opts -->
                        </div>
                        <!--/ post_topbar -->
                        <?php if (isset($post->post_id)) { ?>
                            <div class="post_content">
                                <p> <?= h($post->content) ?> </p>
                                <div class="retweet-bar col-md-11 offset-md-1">
                                    <div class="retweet_bar">
                                        <div class="post_topbar">
                                            <div class="usy-dt">
                                                <?= $this->Html->image("profile/" . h($post->author_retweet->image));?>
                                                <div class="usy-name">
                                                    <h6>
                                                        <?php
                                                            $AuthorId = h($post->author_retweet->id);
                                                            echo $this->Html->link(h($post->author_retweet->first_name). " " . h($post->author_retweet->last_name), [
                                                                    'controller' => 'users',
                                                                    'action' => 'view',
                                                                    $AuthorId
                                                                ]);
                                                        ?>
                                                    </h6>
                                                    <span>
                                                        <i class="fa fa-clock-o"></i>
                                                        <?= $this->Time->format(h($post->retweet->created)) ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_content" onClick="document.location.href='../../posts/view/<?=$post->post_id?>'">
                                            <?php
                                                // check if post has text or image
                                                if (empty($post->image)) {
                                                    // check if the origin of post has been deleted
                                                    if ($post->retweet->is_deleted == 1) {
                                                        echo '<p style="color:#e44d3a;"> Content is not available</p>';
                                                    } else {
                                                        echo '<p>';
                                                            echo h($post->retweet_content);
                                                        echo '</p>';
                                                    }
                                                } else {
                                                    if ($post->retweet->is_deleted == 1) {
                                                        echo '<p style="color:#e44d3a;"> Content Not Available</p>';
                                                    } else {
                                                        echo '<p>';
                                                            echo h($post->retweet_content);
                                                        echo '</p>';
                                                        echo $this->Html->image("posts/" . h($post->image));
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <!--/ post_content -->
                                    </div>
                                    <!--/ retweet_bar -->
                                </div><!-- end of retweet-bar col-md-11 -->
                            </div><!-- end of post content -->
                        <?php } else { ?>
                            <div class="post_content" onClick="document.location.href='../../posts/view/<?=$post->id?>'">
                                <?php
                                    if (empty($post->image)) {
                                        echo '<p>';
                                            echo h($post->content);
                                        echo '</p>';
                                    } else {
                                        echo '<p>';
                                            echo h($post->content);
                                        echo '</p>';
                                        echo $this->Html->image("posts/" . h($post->image));
                                    }?>
                            </div>
                            <!--/ post_content -->
                        <?php } ?>
                        <div class="job-status-bar">
                            <ul class="like-com">
                                <?php $likeId = $id; ?>
                                <li style="margin-right: 0px !important;">
                                    <p class="countLikes<?=$likeId?>"><?= sizeof($post->likes) ?></p>
                                </li>
                                <?php
                                    $isLiked = false;
                                    for ($i = 0; $i < sizeof($post->likes); $i++) {
                                        $isLiked = ($post['likes'][$i]['user_id'] == $authUser['id'] ? true : false);
                                    } ?>
                                    <?php if ($isLiked) { ?>
                                        <li class="btnLike">
                                            <p style="color:#e44d3a !important" class="com comLabel commentLabel<?=$likeId?>" onclick="unlikePost(<?=$likeId?>);">
                                                <i class="fa fa-thumbs-up"></i>
                                                Like
                                            </p>
                                        </li>
                                    <?php } else { ?>
                                        <li class="btnLike">
                                            <p class="com comLabel commentLabel<?=$likeId?>" onclick="likePost(<?=$likeId?>);">
                                                <i class="fa fa-thumbs-up"></i>
                                                Like
                                            </p>
                                        </li>
                                    <?php } ?>
                                <li style="margin-right: 0px !important;">
                                    <p class="countComments<?=$likeId?>">
                                        <?= sizeof($post->comments)?>
                                    </p>
                                </li>
                                <li>
                                    <a id="<?=$id?>" class="com commentShow" >
                                        <i class="fa fa-comment"></i>
                                            Comment
                                    </a>
                                </li>
                                <li>
                                    <?= $this->Html->link('<i class="fa fa-retweet"></i> ' .sizeof($post->count_retweets). ' Retweet', [
                                            'controller' => 'posts',
                                            'action' => 'retweet',
                                            $id], [
                                            'escape' => false,
                                            'class' => 'com'
                                            ]);
                                    ?>
                                </li>
                            </ul>
                        </div>
                        <!--/ job-status-bar -->
                    </div>
                    <!--/ post-bar-->
                    <div class="comment-section" style="padding-left:15px !important;">
                        <div id="updateComment<?=$id?>">
                        <?php
                        if (sizeof($post->comments) > 0) { ?>
                            <?php for($i = 0; $i < sizeof($post->comments); $i++) { ?>
                                <div class="comment-sec<?=$id?>" id="comment<?=$post['comments'][$i]['id']?>">
                                    <ul>
                                        <li>
                                            <div id="commentSection" class="comment-list">
                                                <div class="bg-img">
                                                    <?= $this->Html->image("profile/" . h($post['comments'][$i]['author_comment']['image']));?>
                                                </div>
                                                <div class="comment">
                                                    <h3>
                                                        <?php
                                                            $commentUserId = $post['comments'][$i]['users']['id'];
                                                            echo $this->Html->link(h($post['comments'][$i]['author_comment']['first_name']) . " " . h($post['comments'][$i]['author_comment']['last_name']), [
                                                                    'controller' => 'users',
                                                                    'action' => 'view',
                                                                    $commentUserId ]);
                                                            if ($authUser['id'] ==$post['comments'][$i]['user_id']) {
                                                                $commentId = $post['comments'][$i]['id'];
                                                        ?>
                                                            <i id="<?= $commentId ?>" data-id="<?= h($post['comments'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                                                <?php
                                                                    echo $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                                                            'escape' => false,
                                                                            'id' => $commentId,
                                                                            'data-id' => $post['id'],
                                                                            'class' => 'delButton delBtn'
                                                                        ]);
                                                            } ?>
                                                    </h3>
                                                    <span> <i class="fa fa-clock-o"></i> <?= $this->Time->format(h($post['comments'][$i]['created']))?> </span>
                                                        <div class="comment-list-content">
                                                            <p id="contentId<?=$post['comments'][$i]['id']?>">
                                                                <?= h($post['comments'][$i]['content'])?>
                                                            </p>
                                                        <div>
                                                </div>
                                                <!--/ comment -->
                                            </div>
                                            <!--/ comment-list -->
                                        </li>
                                    </ul>
                                </div>
                                <!--/ comment-sec -->
                            <?php } ?>
                        <?php } ?>
                        </div><!--/ updateComment -->
                        <div class="row post-comment" style="padding-bottom:30px;">
                                <div class="bg-img">
                                    <?= $this->Html->image("profile/" . h($authUser['image']));?>
                                </div>
                            <div class="comment_box">
                                <?= $this->Form->create('Comment', [
                                        'type' => 'get',
                                        'id' => 'saveComment',
                                        'class' => 'saveComment'
                                    ])
                                ?>
                                <?= $this->Form->control('postid', [
                                        'type' => 'hidden',
                                        'value' => $post->id,
                                        'label' => false
                                    ])
                                ?>
                                <?= $this->Form->control('userid', [
                                        'type' => 'hidden',
                                        'value' => $authUser['id'],
                                        'label' => false
                                    ])
                                ?>
                                <?php
                                    $lastComment = end($comment); // get the last comment
                                    $comId = $lastComment['id'];
                                ?>
                                <?= $this->Form->control('commentid', [
                                    'type' => 'hidden',
                                    'value' => $comId,
                                    'label' => false,
                                    'count' => 1,
                                    'class' => 'countComment'.$post->id
                                    ])
                                ?>
                                <?= $this->Form->control('content', [
                                        'placeholder' => 'Post a comment',
                                        'maxlength' => 140,
                                        'label' => false
                                    ])
                                ?>
                            </div>
                            <div>
                                <?= $this->Form->control('Send', [
                                        'type' => 'submit',
                                        'label' => false,
                                        'id' => 'submitComment',
                                        'class'=>'comment_box2'
                                    ])
                                ?>
                                <?= $this->Form->end()?>
                            </div>
                        </div>
                        <!--/ post-comment-->
                    </div>
                    <!--/ comment-section-->
                    <?php endforeach; ?>
                </div>
            </div>
            <!--/ main-ws-sec -->
        </div>
        <!--/ col-lg-6 col-md-8 no-pd -->
    </div>
    <!--/ row -->
</div>
<!--/ main-section-data -->

<!-- Edit Comment Modal -->
<!-- Modal -->
<div class="modal fade" id="editComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Edit Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </div>
            <!--/ modal-header -->
            <div class="modal-body">
                <?= $this->Form->create('Comment', [
                        'type' => 'get',
                        'id' => 'editComment',
                        'class' => 'editComment'
                    ])
                ?>
                <?= $this->Form->control('postid', [
                        'id' => 'commentId',
                        'type' => 'hidden',
                        'label' => false
                        ])
                ?>
                <div class="form-group">
                    <?= $this->Form->control('content', [
                            'type' => 'textarea',
                            'rows' => '3',
                            'cols' => '60',
                            'id' => 'commentContent',
                            'class' => 'form-control',
                            'maxlength' => 140,
                            'label' => false
                        ])
                    ?>
                    <small class="form-text text-muted">
                        <span style="float:left;" id="commentRemainingCharacter">140</span>
                    </small>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('Save Changes', [
                            'type' => 'submit',
                            'label' => false,
                            'id' => 'submitEditedComment',
                            'class'=>'comment_box2'
                        ])
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <!--/ modal-body -->
        </div>
        <!--/ modal-content -->
    </div>
    <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<script type="text/javascript">
$(document).ready(function () {
    $(".saveComment").submit(function(e){

        //prevent page from loading
        e.preventDefault();
        let formData = $(this).serializeArray();
        let values = {};

        // get the values from serialize array
        $(formData).each(function(i, field){
            values[field.name] = field.value;
        });
        // Value Retrieval Function
        let getValue = function (valueName) {
            return values[valueName];
        };

        let id = getValue("postid");
        let userid = getValue("userid");
        let idcomment = getValue("commentid");
        let content = getValue("content");

        let countOrig = parseInt($('input[name=commentid').attr('count'));
        let commentId = parseInt(idcomment) + countOrig;
        let totalComments = parseInt($('.countComments' + id).text());
        let subComment = totalComments + 1;
        let count = parseInt($('input[name=commentid').attr('count')) + 1;

        // disable send button after submit
        $("#submitComment").attr("disabled", true);
        if(content == '') {
            $('input[name=content').prop('required', true);
            $("#submitComment").attr("disabled", false);
        } else {
            $.ajax({
                type: 'POST',
                url: "../../comments/add/",
                data: {postid: id, userid: userid, content: content},
                headers: {'X-CSRF-Token': '<?= h($this->request->getParam('_csrfToken')); ?>'},
                complete: function(data,textStatus,xhr) {
                    $('.countComment' + id).attr('count', count);
                    // enable send button after submit
                    $("#submitComment").attr("disabled", false);
                    // increment number of comment
                    $('.countComments' + id).text(subComment)
                    $('input[name=content').val('');
                    $('input[name=content').prop('required', true);
                    $('#updateComment' + id).append(`
                    <div class="comment-sec`+id+`" id="comment`+commentId+`">
                        <ul>
                            <li>
                                <div id="commentSection" class="comment-list">
                                    <div class="bg-img">
                                        <?= $this->Html->image("profile/". $authUser['image']);?>
                                    </div>
                                    <div class="comment">
                                        <h3>
                                        <?php
                                        echo $this->Html->link(h($authUser['first_name']) . " " . h($authUser['last_name']), [
                                                'controller' => 'users',
                                                'action' => 'view',
                                                $authUser['id']
                                        ]);?>
                                            <i id="`+commentId+`" data-id="` + content + `" class="editButton editBtn fa fa-edit"></i>
                                            <?= $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                                    'escape' => false,
                                                    'id' => "`+commentId+`",
                                                    'data-id' => "`+id+`",
                                                    'class' => 'delButton delBtn'
                                                ]);
                                            ?>
                                        </h3>
                                        <span>
                                            <i class="fa fa-clock-o"></i>
                                            <?= date("d.m.Y, H:i"); ?>
                                        </span>
                                        <div class="comment-list-content">
                                            <p id="contentId`+commentId+`">
                                            </p>
                                        <div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    `);
                    $('#contentId' + commentId).text(content);
                }
            });
        }

        return false;
    });
});
</script>
