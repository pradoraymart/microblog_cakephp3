<div class="main-section-data">
    <div class="row">
        <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
            <div class="main-ws-sec">
                <div class="posts-section">
                <?php foreach ($posts as $post): ?>
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="usy-dt">
                            <?= $this->Html->image("profile/" . h($authUser['image']));?>
                                <div class="usy-name">
                                    <h3>
                                        <?php
                                            $userId = $authUser['id'];
                                            echo $this->Html->link(h($authUser['first_name']). " " . h($authUser['last_name']), [
                                                    'controller' => 'users',
                                                    'action' => 'view',
                                                    $userId
                                                ]);
                                        ?>
                                    </h3>
                                    <span>
                                        <?= h($authUser['username'])?>
                                    </span>
                                </div>
                                <!--/ usy-name -->
                            </div>
                            <!--/ usy-dt -->
                        </div>
                        <!--/ post_topbar -->
                        <div class="post_content">
                            <?php if (isset($post->post_id)) {?>
                                <?= $this->Form->create($post, ['enctype' => 'multipart/form-data']);?>
                                <div class="form-group">
                                    <?= $this->Form->textarea('content', [
                                            'rows' => '2',
                                            'cols' => '100',
                                            'maxlength' => 140,
                                            'class' => 'form-control',
                                            'required' => false,
                                            'value' => $post->content,
                                            'placeholder' => 'Add caption or comment...'
                                        ]);
                                    ?>
                                    <small class="form-text text-muted"  style="float:left;">
                                        <span id="remainingCharacter">140</span>
                                        Character(s) Remaining
                                    </small><br>
                                </div>
                                <!--/ form-group -->
                                <div class="retweet-bar col-md-11 offset-md-1">
                                    <div class="retweet_bar">
                                        <div class="post_topbar">
                                            <div class="usy-dt">
                                                <?= $this->Html->image("profile/" . h($post->author_retweet->image));?>
                                                <div class="usy-name">
                                                    <h6>
                                                    <?php
                                                        $authorId = $post->author_retweet->id;
                                                        echo $this->Html->link(h($post->author_retweet->first_name). " " . h($post->author_retweet->last_name), [
                                                                'controller' => 'users',
                                                                'action' => 'view',
                                                                $authorId
                                                            ]);
                                                    ?>
                                                    </h6>
                                                    <span>
                                                        <i class="fa fa-clock-o"></i>
                                                        <?= $this->Time->format(h($post->retweet->created))?>
                                                    </span>
                                                </div>
                                                <!-- / usy-name -->
                                            </div>
                                            <!-- / usy-dt -->
                                        </div>
                                        <!-- / post_topbar -->
                                        <div class = "post_content" onClick="document.location.href='../posts/view/<?=$post->post_id?>'">
                                            <?php if (empty($post->image)) {
                                                    echo '<p>';
                                                        echo h($post->retweet_content);
                                                    echo '</p>';
                                                } else {
                                                    echo '<p>';
                                                        echo h($post->retweet_content);
                                                    echo '</p>';
                                                    echo $this->Html->image("posts/" . h($post->image));
                                                }?>
                                        </div>
                                        <!-- / post_content -->
                                    </div>
                                    <!-- / retweet-bar-->
                                </div>
                                <!-- / retweet-bar col-md-11 -->
                            <div class="form-group">
                                <?php
                                    echo $this->Form->button('Save',
                                        ['action' => 'edit',
                                        'class'=>'btn btn-primary float-right',
                                        'onclick' => "this.form.submit(); this.disabled=true;"]);
                                    echo $this->Form->end();
                                ?>
                            </div>
                            <?php } else { ?>
                                <?= $this->Form->create($post, ['enctype' => 'multipart/form-data']);?>
                                <div class="form-group">
                                    <label for="content">
                                    <?= $this->Form->textarea('content', [
                                            'rows' => '2',
                                            'cols' => '100',
                                            'maxlength' => 140,
                                            'class' => 'form-control',
                                            'required' => false,
                                            'value' => $post->content,
                                            'placeholder' => 'Add caption or comment...'
                                        ]);
                                    ?>
                                    <small class="form-text text-muted"  style="float:left;">
                                        <span id="remainingCharacter">140</span>
                                            Character(s) Remaining
                                    </small><br>
                                </div>
                                <div class="form-row">
                                    <?php if (isset($post->image)) { ?>
                                        <div class="col form-group">
                                            <?= $this->Form->control('image', [
                                                    'type' => 'file',
                                                    'title' => $post->image,
                                                    'value' => $post->image,
                                                    'label' => false
                                                ])
                                            ?>
                                        </div>
                                        <!--/ col form-group -->
                                    <?php } ?>
                                    <div class="col form-group">
                                        <?php
                                            echo $this->Form->button('Save', [
                                                'action' => 'edit',
                                                'class'=>'btn btn-primary float-right',
                                                'onclick' => "this.form.submit(); this.disabled=true;"
                                                ]);
                                            echo $this->Form->end();
                                        ?>
                                    </div>
                                    <!--/ col form-group -->
                                </div>
                                <!--/ form-row -->
                            <?php } ?>
                        </div>
                        <!--/ post_content -->
                    </div>
                    <!--/ post-bar -->
                    <?php endforeach; ?>
                    <!--/ foreach -->
                </div>
                <!--/ main-section -->
            </div>
            <!--/ main-ws-sec -->
        </div>
        <!--/ col-lg-6 col-md-8 no-pd -->
    </div>
    <!--/ row -->
</div>
<!--/ main-section-data -->
