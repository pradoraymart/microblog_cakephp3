<div class="main-section-data">
    <div class="row">
        <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
            <div class="main-ws-sec">
                <div class="posts-section">
                    <?php foreach ($posts as $post): ?>
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="usy-dt">
                                <?= $this->Html->image("profile/" . h($authUser['image']));?>
                                <div class="usy-name">
                                    <h3>
                                    <?php
                                        $userId = $authUser['id'];
                                        echo $this->Html->link(h($authUser['first_name']). " " . h($authUser['last_name']), [
                                                'controller' => 'users',
                                                'action' => 'view',
                                                $userId
                                            ])
                                    ?>
                                    </h3>
                                    <span> <?= h($authUser['username']) ?> </span>
                                </div>
                                <!--/ usy-name -->
                            </div>
                            <!-- /usy-dt -->
                        </div>
                        <!--/ post_topbar -->
                        <div class="post_content">
                            <?= $this->Form->create('Post', array('enctype' => 'multipart/form-data'));?>
                            <div class="form-group">
                                <label for="content">
                                <?= $this->Form->textarea('retweet_content', [
                                        'rows' => '2',
                                        'cols' => '100',
                                        'maxlength' => 140,
                                        'id' => 'PostRetweetContent',
                                        'class' => 'form-control',
                                        'placeholder' => 'Add caption or comment...'
                                    ]);
                                ?>
                                <small class="form-text text-muted" style="float:left;" >
                                    <span id="remainingCharacter">140</span>
                                        Character(s) Remaining
                                </small><br>
                            </div>
                            <!--/ form-group -->
                            <?php if (isset($post->post_id)) {?>
                            <div class="retweet-bar col-md-11 offset-md-1">
                                <div class="retweet_bar">
                                    <div class="post_topbar">
                                        <div class="usy-dt">
                                            <?= $this->Html->image("profile/" . h($post->author_retweet->image));?>
                                            <div class="usy-name">
                                                <h6>
                                                <?php
                                                    $authorId = $post->author_retweet->id;
                                                    echo $this->Html->link(h($post->author_retweet->first_name). " " . h($post->author_retweet->last_name), [
                                                            'controller' => 'users',
                                                            'action' => 'view',
                                                            $authorId
                                                        ]);
                                                ?>
                                                </h6>
                                                <span>
                                                    <i class="fa fa-clock-o"></i>
                                                    <?= $this->Time->format(h($post->created)) ?>
                                                </span>
                                            </div>
                                            <!--/ usy-name -->
                                        </div>
                                        <!--/ usy-dt -->
                                    </div>
                                    <!--/ post_topbar -->
                                    <div class="post_content" onClick="document.location.href='../posts/view/<?=$post->post_id?>'">
                                        <?php
                                            if (empty($post->image)) {
                                                echo '<p>';
                                                    echo h($post->retweet_content);
                                                echo '</p>';
                                            } else {
                                                echo '<p>';
                                                    echo h($post->retweet_content);
                                                echo '</p>';
                                                echo $this->Html->image("posts/" . h($post->image));
                                            } ?>
                                    </div>
                                    <!--/ post_content -->
                                </div>
                                <!--/ retweet_bar -->
                            </div>
                            <!--/ retweet-bar col-md-11 offset-md-1 -->
                            <?php } else { ?>
                            <div class="retweet-bar col-md-11 offset-md-1">
                                <div class="retweet_bar">
                                    <div class="post_topbar">
                                        <div class="usy-dt">
                                            <?= $this->Html->image("profile/" . h($post->user->image));?>
                                            <div class="usy-name">
                                                <h6>
                                                    <?php
                                                        $postAuthorId = $post->user->id;
                                                        echo $this->Html->link(h($post->user->first_name). " " . h($post->user->last_name), [
                                                                'controller' => 'users',
                                                                'action' => 'view',
                                                                $postAuthorId
                                                            ]);
                                                    ?>
                                                </h6>
                                                <span>
                                                    <i class="fa fa-clock-o"></i>
                                                    <?= $this->Time->format(h($post->created)) ?>
                                                </span>
                                            </div>
                                            <!--/ uusy-name -->
                                        </div>
                                        <!--/ usy-dt -->
                                    </div>
                                    <!--/ post_topbar -->
                                    <div class="post_content" onClick="document.location.href='../posts/view/<?=$post->id?>'">
                                    <?php
                                        if (empty($post->image)) {
                                            echo '<p>';
                                                echo h($post->content);
                                            echo '</p>';
                                        } else {
                                            echo '<p>';
                                                echo h($post->content);
                                            echo '</p>';
                                            echo $this->Html->image("posts/" . h($post->image));
                                        }?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <?php
                                    echo $this->Form->button('Retweet Post', [
                                            'action' => 'retweet',
                                            'class' => 'btn btn-primary float-right',
                                            'onclick' => "this.form.submit(); this.disabled=true;"
                                        ]);
                                    echo $this->Form->end();
                                ?><br>
                            </div>
                            <!--/ form-group -->
                        </div>
                        <!--/ post_content -->
                    </div>
                    <!--/ post-bar -->
                    <?php endforeach; ?>
                </div>
                <!--/ main-section -->
            </div>
            <!--/ main-ws-sec -->
        </div>
        <!--/ col-lg-6 col-md-8 no-pd -->
    </div>
    <!--/ row -->
</div>
<!--/ main-section-data -->
