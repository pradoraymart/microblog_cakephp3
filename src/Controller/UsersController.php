<?php
// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Validation\Validator;

/**
 * Users Controller
 * This controller will render views from Template/Users/
 *
 * @property App\Model\Entity\User
 */
class UsersController extends AppController
{
    /**
     * beforeFilter Function
     *
     * This function is executed before every action in the controller
     * @param Event $event
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
        $this->set('authUser', $this->Auth->user());

        // Allow users to register, logout and edit profile
        $this->Auth->allow(['register', 'verification', 'edit']);
    }

    /**
     * Search user and posts
     * Display list of post and user based on sent keyword
     * @return void
     */
    public function search()
    {
        //get the search value from the url
        $keyword = $this->request->getQuery('search');

        if (!empty($keyword)) {
            // fetch searched user
            $this->paginate = [
                'order' => [
                    'first_name' => 'asc',
                ],
                'limit' => 4,
                'order' => [
                    'created' => 'desc',
                ],
            ];

            $user = $this->paginate(
                $this->Users->find('all')
                ->where([
                    'AND' => [
                        'is_verified' => 1,
                        'OR' => [
                            'first_name LIKE' => "%" . trim($keyword) . "%",
                            'last_name LIKE' => "%" . trim($keyword) . "%",
                            'username LIKE' => "%" . trim($keyword) . "%",
                        ]
                    ]
                ]),
                ['scope' => 'user_search']
            );
            $postTable = TableRegistry::getTableLocator()->get('Posts');
            $posts = $this->paginate(
                $postTable->find('all')
                    ->where([
                        'OR' => [
                            ['Posts.content LIKE' => "%" . trim($keyword) . "%"],
                            ['Posts.retweet_content LIKE' => "%" . trim($keyword) . "%"],
                        ],
                        ['Posts.is_deleted' => 0]
                    ])
                    ->group('Posts.id')
                    ->contain(['Users', 'AuthorRetweets', 'Retweets', 'Comments.AuthorComments', 'Likes']),
                ['scope' => 'posts_search']
            );

            // get all comments, get last ID record for ajax add comment
            $commentTable = TableRegistry::getTableLocator()->get('Comments');
            $comment = $commentTable->find('all')
                ->where(['is_deleted' => 0])
                ->orWhere(['is_deleted' => 1])
                ->toArray();

            $this->set(compact('user', 'comment'));
            $this->set('_serialize', ['user']);
            $this->set('authUser', $this->Users->get($this->Auth->user('id')));

            $this->set(compact('posts'));
            $this->set('_serialize', ['posts']);
            $this->request->session()->write('redirect', $this->request->here());
        } else {
            //show redirect to page if user did not insert text on search input
            $this->redirect($this->referer());
        }
    }

    /**
     * View user accout information
     *
     * @param int $id // Handles request user id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function view($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }

        $commentTable = TableRegistry::getTableLocator()->get('Comments');
        $comment = $commentTable->find('all')
            ->where(['is_deleted' => 0])
            ->orWhere(['is_deleted' => 1])
            ->toArray();

        $postTable = TableRegistry::getTableLocator()->get('Posts');
        $this->paginate = [
            'limit' => 4,
            'order' => [
                'created' => 'desc',
            ],
        ];
        // display user own posts
        $posts = $this->paginate($postTable->find('all')
            ->where([
                'AND' => [
                    ['Posts.user_id' => $id],
                    ['Posts.is_deleted' => 0] // only undeleted posts
                ]
            ])
            ->contain(['Users', 'Comments.AuthorComments', 'Retweets', 'Likes', 'AuthorRetweets', 'CountRetweets']));
        $this->set(compact('posts','comment'));
        $this->set('_serialize', ['posts']);

        //find user
        $user = $this->Users->find('all')
            ->where(['id' => $id]);

        $followTable = TableRegistry::getTableLocator()->get('Follows');
        // For Follow Button status
        $follow = $followTable->find('all')
            ->where([
                'Follows.following' => $id,
                'Follows.user_id' => $this->Auth->user('id')
                ])
            ->toArray();

        // Get Following
        $followers = $this->Users->find('all')
            ->where([
                'AND' => [
                    ['followJoin.user_id' => $id],
                    ['Users.is_verified' => 1]
                ]
            ])
            ->join([
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['Users.id = followJoin.following']]
            ])
            ->toArray();

        // Get Followers
        $following = $this->Users->find('all')
            ->where([
                'AND' => [
                    ['followJoin.following' => $id],
                    ['Users.is_verified' => 1]
                ]
            ])
            ->join([
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['Users.id = followJoin.user_id']]
            ])
            ->toArray();

        if (!$user) {
            throw new NotFoundException(__('User not Found'));
        }

        $this->set('authUser', $this->Users->get($this->Auth->user('id')));
        $this->request->session()->write('redirect', $this->request->here());
        $this->set(compact('user', 'follow', 'following', 'followers'));
    }

    /**
     * Edit user accout information
     *
     * @param int $id Handles request user id
     * @return view
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function edit()
    {
        $id = $this->Auth->user('id');
        // prevent user to edit profile if not logged in
        if (!$id) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        $user = $this->Users->get($id);
        //show NotFoundException if user id not exists
        if (!$user) {
            throw new NotFoundException();
        }
        if ($this->request->is(['post', 'put'])) {
            $filename = '';
            $myImage = $this->request->getData('image');
            $userImage = $user['image'];
            //check user uploaded image
            if (!empty($myImage['name'])) {
                $uploadData = $myImage;
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT . 'img';
                $filename = time() . '_' . $filename;
                $uploadPath = $uploadFolder . DS . 'profile' . DS . $filename;
                $ext = substr(strtolower(strrchr($myImage['name'], '.')), 1); //get the extension
                $arr_ext = ['jpg', 'jpeg', 'gif', 'png']; //set allowed extensions

                if (in_array($ext, $arr_ext)) {
                    //save the file to the server after successful save
                    move_uploaded_file($uploadData['tmp_name'], $uploadPath);
                } else {
                    $this->Flash->error(__('Unable to add image. Please insert a valid image'));

                    return $this->redirect(['action' => 'edit']);
                }
                $userImage = $filename;
            } else {
                $data = $this->request->getData();
                unset($data['image']);
            }

            //check if user change gender
            $oldGender = $user['gender'];
            $gender = $this->request->getData('gender');
            $checkGender = ($oldGender == $gender ? true : false);
            if ($checkGender == false) {
                if (empty($myImage['name'])) {
                    $imageName = $user['image'];
                    if ($imageName == 'female_user.png' || $imageName == 'male_user.png') {
                        switch ($gender) {
                            case 1:
                                $userImage = 'female_user.png';
                                break;
                            case 0:
                                $userImage = 'male_user.png';
                                break;
                        }
                    }
                }
            }

            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'validate' => 'Update'
            ]);

            $userPass = $this->request->getData('new_password');
            // check password if not empty
            if (!empty($new_password = $userPass) || !empty($this->request->getData('confirm_password'))) {
                $validator = new Validator();
                $validator
                    ->notEmpty('new_password')
                    ->notEmpty('confirm_password')
                    ->add('confirm_password', 'compareWith', [
                        'rule' => ['compareWith', 'new_password'],
                        'message' => 'Passwords do not match.'
                    ]);
                $errors = $validator->errors($this->request->data());
                if (!$errors) {
                    $hasher = new DefaultPasswordHasher();
                    $user->password = $hasher->hash($new_password);
                } else {
                    $this->Flash->error(__('Please confirm your password'));

                    return $this->redirect(['action' => 'edit']);
                }
            }

            $user->image = $userImage;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Successfully Updated'));

                return $this->redirect(['controller' => 'users', 'action' => "view/$id"]);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        $this->set('authUser', $this->Users->get($this->Auth->user('id')));
    }

    /**
     * User registration
     *
     * @param int $id Handles request user id
     * @return view
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function register()
    {
        if (!$this->Auth->user()) {
            $userTable = TableRegistry::getTableLocator()->get('Users');
            $user = $userTable->newEntity();
            if ($this->request->is('post')) {
                $gender = $this->request->getData('gender');
                $emailTo = $this->request->getData('email'); // get recipient email for email verification
                $token = Security::hash(Security::randomBytes(32)); // generate token for secure verification

                $user = $userTable->patchEntity($user, $this->request->getData(), [
                    'validate' => 'Register'
                ]);
                $user->token = $token;
                //set user profile image based on gender
                $user->image = 'male_user.png';
                if ($gender == '1') {
                    $user->image = 'female_user.png';
                }
                if ($userTable->save($user)) { //if it save send a flash message
                    $this->Flash->success(__('You are almost there! An email verification link has been sent to ' . $emailTo . ' .Please click the link to verify'));

                    //send email using cakephp Email
                    $Email = new Email();
                    $Email
                        ->template('emailVerification') //sent this view to email
                        ->emailFormat('html')
                        ->subject('Confirm Registration')
                        ->to($emailTo)
                        ->viewVars(['token' => $token]) //pass data to email_verification.ctp
                        ->send();

                    return $this->redirect(['action' => 'login']);
                } else {
                    //some reason they cant register
                    $this->Flash->error('Registration unsuccessful');
                }
            }
            $this->set(compact('user'));
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    /**
     * Verifies user account from email
     * @param int $token User ID
     * @param string $token A random generated string that serves as a key
     * @return view
     */
    public function verification($token)
    {
        $users = $this->Users->find('all')
            ->where(['token' => $token])
            ->toArray();
        if (!$users) {
            throw new NotFoundException();
        }

        if ($users[0]['is_verified'] != '1') {
            $user = $this->Users->get($users[0]['id']);
            $user->is_verified = 1;

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Account successfully verified!'));

                return $this->redirect('/login');
            } else {
                $this->Flash->error(__('An error has occured while saving data'));
            }
        } else {
            $this->Flash->error(__('Account was already verified!'));

            return $this->redirect('/login');
        }
    }

    /**
     * User logout
     *
     * @return view login
     */
    public function logout()
    {
        $this->request->session()->destroy('redirect');

        return $this->redirect($this->Auth->logout());
    }

    /**
     * User Login
     *
     * @return view posts/index
     */
    public function login()
    {
        if (!$this->Auth->user()) {
            $login = $this->Users->newEntity();
            if ($this->request->is('post') && !empty($this->request->getData())) {
                $check_login = $this->Users->patchEntity($login, $this->request->getData(), [
                    'validate' => 'login'
                ]);
                if ($check_login->errors()) {
                    $this->Flash->error(__('Please fill up the required fields'));
                } else {
                    $user = $this->Auth->identify();
                    if ($user) {
                        $this->Auth->setUser($user);

                        return $this->redirect($this->Auth->redirectUrl());
                    } else {
                        $this->Flash->error(__('Username or password is incorrect'));
                    }
                }
            }
            $this->set('login', $login);
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }
}
