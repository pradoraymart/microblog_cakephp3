<?php
// src/Controller/CommentsController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Comments Controller
 *
 * @property App\Model\Entity\Comment
 */
class CommentsController extends AppController
{
    /**
     * This function is executed before every action in the controller
     * Allows user to add, edit and delete posts
     * @param Event $event
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('RequestHandler');
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
        $this->Auth->allow('add', 'edit', 'delete');
    }

    /**
     * Add user comments based on passed post id using ajax
     *
     * @return void
     */
    public function add()
    {
        $this->autoRender = false; // no view to render
        // check comment author
        $checkUser = $this->Auth->user('id');
        // show 403 ForbiddenException if other user try to add comments on other profile
        if ($checkUser != $this->request->getData('userid')) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        $commentsTable = TableRegistry::get('Comments');
        $comment = $commentsTable->newEntity();
        //check if user inserted a comment
        if ($this->request->is('post')) {
            $comment->post_id = $this->request->getData('postid');
            $comment->user_id = $this->request->getData('userid');
            $comment->content = $this->request->getData('content');
            if ($commentsTable->save($comment)) {
                $this->render('index', 'ajax');
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * Add user comments based on passed post id
     *
     * @param int $id comment id
     * @return void
     */
    public function edit($id = null)
    {
        $this->autoRender = false; // no view to render

        $commentsTable = TableRegistry::get('Comments');
        // check if comment exists
        $comments = $commentsTable->find('all', [
            'conditions' => [
                'AND' => [
                    ['id' => $id],
                    ['user_id' => $this->Auth->user('id')] // only his/her comment can access
                ]
            ],
        ])
        ->toArray();
        if (!$comments) {
            throw new NotFoundException();
        }

        // check comment and comment author
        $checkUser = $commentsTable->find('all')
            ->where([
                'AND' => [
                    'id' => $id,
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
        // show 403 ForbiddenException if other user try to edit the comment
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        $id = $this->request->getData('postid');
        $comment = $commentsTable->get($id);

        $data = ['content' => $this->request->getData('content')];
        $comment = $commentsTable->patchEntity($comment, $data, [
            'fieldList' => ['content'] // only content will be modified
        ]);
        if ($commentsTable->save($comment)) {
            $this->render('index', 'ajax');
        } else {
            $this->redirect($this->referer());
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
    }

    /**
     * delete user comments based on passed comment id
     *
     * @param int $id get comment id
     * @return void
     */
    public function delete($id = null)
    {
        $this->autoRender = false; // no view to render

        $commentsTable = TableRegistry::get('Comments');
        // check if comment exists
        $comments = $commentsTable->find('all', [
            'conditions' => [
                'AND' => [
                    ['id' => $id],
                    ['user_id' => $this->Auth->user('id')] // only his/her comment can access
                ]
            ],
        ])
        ->toArray();
        if (!$comments) {
            throw new NotFoundException();
        }

        // check comment and comment author
        $checkUser = $commentsTable->find('all')
            ->where([
                'AND' => [
                    'id' => $id,
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
        // show 403 ForbiddenException if other user try to edit the comment
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        $comment = $commentsTable->get($id);

        $data = ['is_deleted' => 1];
        $comment = $commentsTable->patchEntity($comment, $data, [
            'fieldList' => ['is_deleted'] // only content will be modified
        ]);
        if ($commentsTable->save($comment)) {
            $this->render('index', 'ajax');
        } else {
            $this->redirect($this->referer());
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
    }
}
