<?php
// src/Controller/FollowsController.php

namespace App\Controller;

/**
 * Follows Controller
 *
 * @property App\Model\Entity\Follow
 */
class FollowsController extends AppController
{
    /**
     * Follow other users
     *
     * @param int $followId
     * @return view users/view
     */
    public function follow($followId)
    {
        // get current logged in user
        $id = $this->Auth->user('id');

        // avoid user to follow his/her own account
        if ($followId != $id) {
        // avoid following the already followed user
            $checkFollow = $this->Follows->find('all')
                ->where([
                    'AND' => [
                        'user_id' => $id,
                        'following' => $followId]
                    ])
                ->toArray();
            // if user is not yet following this $id
            if (!$checkFollow) {
                $follow = $this->Follows->newEntity();
                $follow = $this->Follows->patchEntity($follow, $this->request->getData());

                // set data to be saved
                $follow->user_id = $id;
                $follow->following = $followId;

                if ($this->Follows->save($follow)) {
                    $this->Flash->success(__('Successfully followed user'));

                    return $this->redirect(['controller' => 'users', 'action' => "view/$followId"]);
                }
                $this->Flash->error(__('Error following user, Please try again'));
            }
            $this->Flash->error(__('Already following the user'));

            return $this->redirect(['controller' => 'users', 'action' => "view/$followId"]);
        } else {
            $this->Flash->error(__('Method not allowed, you cant follow/unfollow yourself'));
        }

        return $this->redirect(['controller' => 'users', 'action' => "view/$followId"]);
    }

    /**
     * Unfollow other users
     *
     * @param int $followId User Id
     * @return view usesrs/view
     */
    public function unfollow($followId)
    {
        // get current logged in user
        $id = $this->Auth->user('id');

        // avoid user to follow his/her own account
        if ($followId != $id) {
        // avoid following the already followed user
            $checkFollow = $this->Follows->find('all')
                ->where([
                    'AND' => [
                        'user_id' => $id,
                        'following' => $followId]
                    ])
                    ->toArray();
            //get the primary key id of the record
            $followTableId = $checkFollow[0]['id'];
            // check the record is still exists else delete the record
            if ($checkFollow) {
                $post = $this->Follows->get($followTableId);

                // unfollow user by deleting record on Follows table
                if ($this->Follows->delete($post)) {
                    $this->Flash->success(__('Successfully unfollowed user'));

                    return $this->redirect(['controller' => 'users', 'action' => "view/$followId"]);
                }
                $this->Flash->error(__('Error unfollowing user, Please try again'));
            }
            $this->Flash->error(__('Already unfollowed the user'));
        } else {
            $this->Flash->error(__('Method not allowed, you cant follow/unfollow yourself'));
        }

        return $this->redirect(['controller' => 'users', 'action' => "view/$followId"]);
    }
}
