<?php
// src/Controller/PostsController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 * This controller will render views from Template/Posts/
 *
 * @property App\Model\Entity\Post
 */
class PostsController extends AppController
{

    /**
     * This function is executed before every action in the controller
     * Allows user to add, edit and delete posts
     *
     * @param Event $event
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Paginator');
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }

        $this->Auth->allow('add', 'edit', 'delete', 'search');
    }

    /**
     * Displays the index view as home page
     * Allows user to save text and image
     *
     * @return view posts/index
     */
    public function index()
    {
        $userTable = TableRegistry::getTableLocator()->get('Users');
        // set pagination limit and order
        $this->paginate = [
                'limit' => 4,
                'order' => ['created' => 'desc'],
            ];
        // paginate posts
        $posts = $this->paginate(
            $this->Posts->find('all')
                ->where([
                    'OR' => [ //display own and followed user posts
                        ['followJoin.user_id' => $this->Auth->user('id')],
                        ['Users.id' => $this->Auth->user('id')]
                    ],
                    ['Posts.is_deleted' => 0]
                ])
                ->join([
                    'table' => 'follows',
                    'alias' => 'followJoin',
                    'type' => 'LEFT',
                    'conditions' => ['Posts.user_id = followJoin.following']
                ])
                ->group('Posts.id')
                ->contain([
                    'Comments.AuthorComments',
                    'Users',
                    'AuthorRetweets',
                    'Retweets',
                    'Likes',
                    'CountRetweets'])
        );
        // Get list Following
        $following = $userTable->find('all')
            ->where(['followJoin.user_id' => $this->Auth->user('id')])
            ->join([
                'table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['Users.id = followJoin.following'],
            ])
            ->toArray();

        // Get list Followers
        $followers = $userTable->find('all')
            ->where(['followJoin.following' => $this->Auth->user('id')])
            ->join([
                'table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['Users.id = followJoin.user_id'],
            ])
            ->toArray();

        // get all comments, get last ID record for ajax add comment
        $commentTable = TableRegistry::getTableLocator()->get('Comments');
        $comment = $commentTable->find('all')
            ->where(['is_deleted' => 0])
            ->orWhere(['is_deleted' => 1])
            ->toArray();

        $this->set(compact('comment', 'posts', 'following', 'followers'));
        $this->set('_serialize', ['posts']);

        $post = $this->Posts->newEntity();
        $this->set(compact('post'));
        $this->request->session()->write('redirect', $this->request->here());

        //save user posts
        if ($this->request->is('post')) { // check type of request
            // check comment author
            $checkUser = $this->Auth->user('id');
            $myId = $this->request->getData('userid');
            // show 403 ForbiddenException if other user try to add comments on other profile
            if ($checkUser != $myId) {
                throw new UnauthorizedException(); // unauthorized access to the resource
            }

            $myPost = $this->request->getData('content');
            $myImage = $this->request->getData('image');

            // check if user inserted a data
            if (!empty($myPost) || !empty($myImage)) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $fileName = '';
                $uploadData = $myImage;
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT . 'img';
                $uploadPath = $uploadFolder . DS . 'posts' . DS . $filename;

                if (!empty($myImage['name'])) {
                    $ext = substr(strtolower(strrchr($myImage['name'], '.')), 1); //get the extension
                    $arr_ext = ['jpg', 'jpeg', 'gif', 'png']; //set allowed extensions

                    if (in_array($ext, $arr_ext)) {
                        //save the file to the server after successful save
                        move_uploaded_file($uploadData['tmp_name'], $uploadPath);
                    } else {
                        $this->Flash->error(__('Unable to add your post. Please insert a valid image'));

                        return $this->redirect(['action' => 'index']);
                    }
                }

                $post->user_id = $this->Auth->user('id');
                $post->image = $filename;
                $post->is_deleted = '0';

                if ($this->Posts->save($post)) {
                    $this->Flash->success(__('Your post has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Unable to add your post.'));
            } else {
                $this->Flash->error(__('Please insert your tweet first'));
            }
        }

        $this->set('authUser', $userTable->get($this->Auth->user('id')));
        $this->set('post', $post);
    }

    /**
     * Retweet User Post
     *
     * @param int $id Handles request post id to be retweeted
     * @return view posts/retweet
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     * be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function retweet($id = null)
    {
        $userTable = TableRegistry::getTableLocator()->get('Users');

        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // find the post and check if exists
        $post = $this->Posts->get($id);
        if (!$post) {
            throw new NotFoundException();
        }
        $posts = $this->Posts->find('all', [
            'conditions' => [
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.is_deleted' => 0]
                ]
            ]])
            ->orwhere([
                ['Retweets.is_deleted' => 0], // getting retweeted posts
                ['Retweets.is_deleted' => null]]) // null or 0])
            ->contain(['Retweets', 'Users', 'AuthorRetweets'])
            ->toArray();
        //show NotFoundException if post exist
        if (!$posts) {
            throw new NotFoundException();
        }

        $postId = $post['post_id']; // get id post id of retweeted post
        $postOrigin = $this->Posts->find(
            'all',
            ['conditions' => ['Posts.id' => $postId], ['Posts.author_id' => null]]
        )->toArray();

        $retweet = $this->Posts->newEntity();
        $checkPostOrigin = ((!empty($postOrigin)) ? true : false );

        //check if post or put request
        if ($this->request->is(['post', 'put'])) {
            // check if retweeing post was already retweeted
            if (($checkPostOrigin == false) && (!empty($posts) == true)) {
                //set data to be save from unretweeted posts
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'post_id' => $id,
                    'content' => $this->request->getData('retweet_content'),
                    'image' => $post['image'],
                    'author_id' => $post['user_id'],
                    'retweet_content' => $post['content'],
                    'is_deleted' => 0
                ];
            } else {
                //set data to be save from retweeted posts
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'post_id' => $postOrigin[0]['id'],
                    'content' => $this->request->getData('retweet_content'),
                    'image' => $postOrigin[0]['image'],
                    'author_id' => $postOrigin[0]['user_id'],
                    'retweet_content' => $postOrigin[0]['content'],
                    'is_deleted' => 0
                ];
            }
            $post = $this->Posts->patchEntity($retweet, $data);

            // save data
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your post has been saved.'));

                // redirect to index or home page
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Unable to add post, Please try again'));
        }
        // get and pass the current logged in user credentials

        $this->set('authUser', $userTable->get($this->Auth->user('id')));
        $this->set(compact('posts'));
    }

    /**
     * Like post function
     *
     * @param int $id // Handles request post id for like post
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     * be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     * @return void
     */
    public function like($id = null)
    {
        $this->autoRender = false; // No view to render
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        $likeTable = TableRegistry::getTableLocator()->get('Likes');
        // check if user already like the post before
        $like = $likeTable->find('all')
            ->where([
                'AND' => [
                    'post_id' => $id,
                    'user_id' => $this->Auth->user('id')
                ]
            ])
            ->toArray();
        // check if post exists and if not deleted
        $post = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.is_deleted' => 0]
                ]
            ])
            ->toArray();
        //show NotFoundException if post id not exists
        if (!$post) {
            throw new NotFoundException();
        }

        if (empty($like)) { // if first time to like the post, create a new record
            $like = $likeTable->newEntity();
            $data = [
                'post_id' => $id,
                'user_id' => $this->Auth->user('id'),
                'status' => 1
            ];
        } else { // update status to liked
            $likeId = $like[0]['id'];
            $like = $likeTable->get($likeId);
            $data = [
                'id' => $like['id'],
                'post_id' => $id,
                'user_id' => $this->Auth->user('id'),
                'status' => 1 // 1 for liked post
            ];
        }
        $like = $likeTable->patchEntity($like, $data);

        // save data
        if ($likeTable->save($like)) {
            $this->render('index', 'ajax');
        } else {
            $this->redirect($this->referer());
            $this->Flash->error(__('Unable to like post'));
        }
    }

    /**
     * Unlike post function
     *
     * @param int $id // Handles request post id for like post
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     * be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     * @return void
     */
    public function unlike($id = null)
    {
        $this->autoRender = false; // No view to render
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        $likeTable = TableRegistry::getTableLocator()->get('Likes');
        //find the liked post id
        $like = $likeTable->find('all')
            ->where([
                'AND' => [
                    'post_id' => $id,
                    'user_id' => $this->Auth->user('id')
                ]
            ])
            ->toArray();
        //check if liked post exists
        if (!$like) {
            throw new NotFoundException();
        }

        // check if post exists and if not deleted
        $post = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.is_deleted' => 0]
                ]
            ])
            ->toArray();
        //show NotFoundException if post id not exists
        if (!$post) {
            throw new NotFoundException();
        }

        $likeId = $like[0]['id'];
        $like = $likeTable->get($likeId);
        //set data to be saved or edited
        $data = [
            'post_id' => $id,
            'user_id' => $this->Auth->user('id'),
            'status' => 0 // 0 for unliked post
        ];
        $like = $likeTable->patchEntity($like, $data);

        //save the data
        if ($likeTable->save($like)) {
            $this->render('index', 'ajax');
        } else {
            $this->redirect($this->referer());
            $this->Flash->error(__('Unable to unlike post'));
        }
    }

    /**
     * Display single post from passed id
     *
     * @param int $id // Handles request post id to be display
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     * be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function view($id = null)
    {
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        $userTable = TableRegistry::getTableLocator()->get('Users');
        // get all comments, get last ID record for ajax add comment
        $commentTable = TableRegistry::getTableLocator()->get('Comments');
        $comment = $commentTable->find('all')
            ->where(['is_deleted' => 0])
            ->orWhere(['is_deleted' => 1])
            ->toArray();

        $posts = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.is_deleted' => 0]
                ]])
            ->contain(['Comments.AuthorComments', 'Users', 'AuthorRetweets', 'Retweets', 'Likes', 'CountRetweets'])
            ->toArray();
        // show NotFoundException if post exist
        if (!$posts) {
            throw new NotFoundException();
        }

        // get and pass the current logged in user credentials
        $this->set('authUser', $userTable->get($this->Auth->user('id')));
        //pass the data to view
        $this->set(compact('posts', 'authUser', 'comment'));
        $this->request->session()->write('redirect', $this->request->here());
    }

    /**
     * Edit single post from passed id
     *
     * @param int $id Handles request post id to be edited
     * @return view posts/edit
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     * be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function edit($id = null)
    {
        $userTable = TableRegistry::getTableLocator()->get('Users');

        // show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // check post and post author
        $checkUser = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.user_id' => $this->Auth->user('id')]
                ]
            ])
            ->toArray();
        // show 403 ForbiddenException if other user try to edit the post
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        // check post if exists
        $checkPost = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.is_deleted' => 0] // only edit undeleted posts
                ]
            ])
            ->contain(['Users', 'AuthorRetweets', 'Retweets'])
            ->toArray();
        // show NotFoundException if post not exist and if other user try to delete the post
        if (!$checkPost) {
            throw new NotFoundException();
        }
        $this->set('posts', $checkPost);

        $post = $this->Posts->get($id);
        if ($this->request->is(['post', 'put'])) {
            $myImage = $this->request->getData('image');
            if (!empty($myImage['name'])) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $fileName = '';
                $uploadData = $myImage;
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT . 'img';
                $uploadPath = $uploadFolder . DS . 'posts' . DS . $filename;
                $ext = substr(strtolower(strrchr($myImage['name'], '.')), 1); //get the extension
                $arr_ext = ['jpg', 'jpeg', 'gif', 'png']; //set allowed extensions

                if (in_array($ext, $arr_ext)) {
                    //save the file to the server after successful save
                    move_uploaded_file($uploadData['tmp_name'], $uploadPath);
                } else {
                    $this->Flash->error(__('Unable to add your post. Please insert a valid image'));

                    return $this->redirect(['action' => 'index']);
                }
                $post->image = $filename;
            } else {
                $post = $this->Posts->patchEntity($post, $this->request->getData(), [
                    'fieldList' => ['content']
                   ]);
            }

            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Successfully updated post'));

                $sessionUrl = $this->request->session()->read('redirect');
                // redirect to index if the referrer is on post view
                if (strpos($sessionUrl, 'posts/view') !== false) {
                    return $this->redirect($sessionUrl); // redirect to posts/index or home page
                } elseif (strpos($sessionUrl, 'users/view') !== false) {
                    return $this->redirect($sessionUrl);
                } else { // redirect to search view
                    return $this->redirect($sessionUrl);
                }
                $this->request->session()->destroy('redirect');
            }
            $this->Flash->error(__('Error updating post, Please try again'));
        }

        // get and pass the current logged in user credentials
        $this->set('authUser', $userTable->get($this->Auth->user('id')));
        $this->set(compact('post', 'authUser'));
    }

    /**
     * Delete single post from passed id
     * Used Manual soft delete
     *
     * @param int $id Handles request post id to be edited
     * @return view based on session
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     * @throws \Cake\Network\Exception\MethodNotAllowedException When the request is not post or put
     */
    public function delete($id = null)
    {
        // show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // check request method
        if ($this->request->is('get')) { //should not be get method to prevent function from web crawlers
            throw new MethodNotAllowedException();
        }

        // check post and post author
        $checkUser = $this->Posts->find('all')
            ->where([
                'AND' => [
                    ['Posts.id' => $id],
                    ['Posts.user_id' => $this->Auth->user('id')]
                ]
            ])
            ->contain(['Users'])
            ->toArray();
        // show 403 ForbiddenException if other user try to edit the post
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        // check post if exists
        $checkPost = $this->Posts->find('all')
            ->where([
                'AND' => [
                    'id' => $id,
                    'is_deleted' => 0 // only delete undeleted posts
                ]
            ]);
        // show NotFoundException if post not exist and if other user try to delete the post
        if (!$checkPost) {
            throw new NotFoundException();
        }

        $post = $this->Posts->get($id);
        // check if request is post
        if ($this->request->is(['post', 'delete'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());

            // set data for deleted post
            $post->is_deleted = 1; // field default is 0, 1 means deleted post
            $post->date_deleted = date("Y-m-d H:i:s");
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Post successfully deleted.'));

                $sessionUrl = $this->request->session()->read('redirect');
                // redirect to index if the referrer is on post view
                if (strpos($sessionUrl, 'posts/view') !== false) {
                    //redirect to index or home page
                    return $this->redirect($this->Auth->redirectUrl());
                } elseif (strpos($sessionUrl, 'users/view') !== false) {
                    return $this->redirect($sessionUrl);
                } else { // redirect to search view
                    return $this->redirect($sessionUrl);
                }
                $this->request->session()->destroy('redirect');
            }
            $this->Flash->error(__('Unable to delete your post.'));
        }
    }
}
