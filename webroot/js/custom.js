$(function() {
    //call function that counts the character tweets
    retweetCounter();

    // Show and hide comments
    $(document).on("click", ".commentShow", function(e) {
        let id = $(this).attr("id");
        e.preventDefault();
        let target = jQuery(".comment-sec" + id);

        if (target.is(':visible')) {
            $('.comment-sec' + id).css('cssText', 'display: none !important');
        } else {
            $('.comment-sec' + id).css('cssText', 'display: block !important');
        }
    });

    //Edit Comments
    $("#editComment").submit(function(e) {
        //prevent page from loading
        e.preventDefault();
        //disable send button after submit
        var formData = $('form.editComment').serialize();
        var commentPostId = $('#commentId').val();

        $("#submitEditedComment").attr("disabled", true);

        $.ajax({
            type: 'GET',
            url: "../../comments/edit/" + commentPostId,
            data: formData,
            success: function(data, textStatus, xhr) {
                window.location.reload();
            },
            error: function(xhr, textStatus, error) {
                // show the response
                window.location.reload();
            }
        });

        return false;
    });

    $(document).on('click','.editButton',function(e) {
        //prevent page from loading
        e.preventDefault();
        var element = $(this);
        var commentId = element.attr("id");
        var commentContent = element.attr("data-id");

        $("#commentId").val(commentId);
        $("#commentContent").val(commentContent);
        $("#submitComment").prop('id', 'submitEditedComment');
        $('#editComments').modal('show');
    });
});

function likePost(id) {
    let totalLikes = parseInt($('.countLikes' + id).text());
    let addLike = totalLikes + 1;

    $.ajax({
        type: 'GET',
        url: "../../posts/like/" + id,
        success: function(data) {
            $('.commentLabel' + id).css('cssText', 'color: #e44d3a !important');
            $('.commentLabel' + id).attr("onclick", "unlikePost("+ id +")");
            $('.countLikes' + id).text(addLike);
        },
        error: function(data) {
        }
    });
}

function unlikePost(id) {
    let totalLikes = parseInt($('.countLikes' + id).text());
    let subLike = totalLikes - 1;

    $.ajax({
        type: 'GET',
        url: "../../posts/unlike/" + id,
        success: function(data) {
            $('.commentLabel' + id).css('cssText', 'color: #b2b2b2 !important');
            $('.commentLabel' + id).attr("onclick", "likePost("+ id +")");
            $('.countLikes' + id).text(subLike);
        },
        error: function(data){
        }
    });
}

function retweetCounter(){
    let maxLength = 140;
    $('#PostContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
    $('#PostRetweetContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
    $('#commentContent').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#commentRemainingCharacter').text(textlen);
    });
    $('#UserSelfDescription').keyup(function() {
        let textlen = maxLength - $(this).val().length;
        $('#remainingCharacter').text(textlen);
    });
}
